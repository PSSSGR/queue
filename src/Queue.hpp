/*
Filename:Queue.hpp
Name:Sri Padala
Description: A simple queue class implementation.
*/

#ifndef QUEUE__HPP
#define QUEUE__HPP
#include <iostream>
#include <utility>

template <typename T>
class Queue {
public:
    // Default Constructor
    Queue();
    // Parameterized Constructor
    Queue(T firstelem);
    // Copy Constructor
    Queue(const Queue<T>& item);
    // Move Constructor
    Queue(Queue<T>&& item);
    // Destructor
    ~Queue();
    class iterator;
    // function empty, does not throw exceptions
    bool empty()const noexcept{return(size() == 0);}
    // function size, does not throw exceptions
    unsigned int size() const noexcept{return(count);}
    // function front, l-value, throws underflow
    T front();
    // function front, read-only, throws underflow
    const T front()const;
    // function back, l-value, throws underflow
    T back();
    // function back, read-only, throws underflow
    const T back()const;
    // function push, does not throw exceptions
    void push(const T& item);
    // function emplace, does not throw exceptions
    template <class... Args> void emplace (Args&&... args);
    // function pop, throws underflow
    void pop();
    // Copy assignment operator overload
    Queue<T>& operator = (const Queue<T>& item);
    // Move assignment operator overload
    Queue<T>& operator = (Queue<T>&& item);
private:
    // Private data / underlying container
    class Node;
    Node* head_pointer;
    Node* tail_pointer;
    unsigned int count;

};

template <typename T>
class Queue<T>::Node {
public:
    // Functions that you deem necessary
    Node(){}
    Node(T thedata,Node* pointer)
        :data(thedata),next_pointer(pointer){}
    Node* getNextLink() const{return next_pointer;}
    T& getData(){return data;}
    const T& getData()const{return data;}
    void setData(T theData){data = theData;}
    void setNextLink(Node* pointer){next_pointer = pointer;}
private:
    T data;
    Node* next_pointer;
};

template <typename T>
class Queue<T>::iterator {
public:
    friend class Queue<T>;
    // Only need one constructor
    iterator(Node* initial): current(initial){}
    // function prefix increment operator overload
    iterator& operator ++()
    {
        current = current->getNextLink();
        return(*this);
    }
    // function postfix increment operator overload
    const iterator operator++(int){
        iterator temp(current);
        current = current->getNextLink();
        return(temp);
    }
    // function de-reference operator overlaod; l-value
    const T operator*()const{
        return(current->getData());
    }
    // function equivalency operator overload
    bool operator ==(const iterator& rside)const{
        return(current == rside.current);
    }
    // function inequivalency operator overload
    bool operator !=(const iterator& rside)const{
        return(current != rside.current);
    }
private:
    // Resource that iterator manages
    Node* current;
};

// QUEUE IMPLEMENTATION
template<class T>//Default Constructor
Queue<T>::Queue()
    :head_pointer(nullptr),tail_pointer(nullptr),count(0){}

template<class T>
Queue<T>::Queue(T firstelem){
    head_pointer = new Node(firstelem,nullptr);
    tail_pointer = head_pointer;
    count = 1;
}
template<class T>
Queue<T>::Queue(const Queue<T>& item){
    
    head_pointer = item.head_pointer;//common ownership
    tail_pointer = item.tail_pointer;
    count = item.count;
}
template<class T>
Queue<T>::Queue(Queue<T>&& item):head_pointer(item.head_pointer),tail_pointer(item.tail_pointer),count(item.count){
    item.head_pointer = nullptr;//transfering the ownership
    item.tail_pointer = nullptr;
    item.count = 0;
}
template<class T>
Queue<T>::~Queue(){}

template<class T>
void Queue<T>::push(const T& item){
    Node* temp = new Node(item,nullptr);
    if(head_pointer == nullptr){//if the queue is empty
        head_pointer = temp;
        tail_pointer = head_pointer;
    }else{
        temp->setNextLink(tail_pointer);
        tail_pointer = temp;
    }
    count++;//increase the size.
}

template<class T>
template <typename... Args>
void Queue<T>::emplace(Args&&... args){
    push(std::move(T(std::forward<Args>(args) ...)));//unpacking the package and push it.
}

template<class T>
T Queue<T>::front(){//will return the front element of the queue
    if(head_pointer == nullptr){
        throw std::underflow_error("Queue is empty");
    }else{
        return(*(iterator(head_pointer)));
    }
}
template<class T>
const T Queue<T>::front()const{//will return the front element of the queue
    if(head_pointer == nullptr){
        throw std::underflow_error("Queue is empty");
    }else{
        return(*(iterator(head_pointer)));
    }
}
template<class T>
T Queue<T>::back(){//will return the last element of the queue
    if(tail_pointer == nullptr){
        throw std::underflow_error("Queue is empty");
    }else{
        return(*(iterator(tail_pointer)));
    }
}
template<class T>
const T Queue<T>::back()const{//will return the last element of the queue
    if(tail_pointer == nullptr){
        throw std::underflow_error("Queue is empty");
    }else{
        return(*(iterator(tail_pointer)));
    }
}

template<class T>
void Queue<T>::pop(){
    if(head_pointer == nullptr){//if the queue is empty.
        throw std::underflow_error("Queue is empty");
    }else{
        if(head_pointer == tail_pointer){//if there is only one elemnt in the queue.
            Node* temp = head_pointer;
            head_pointer = tail_pointer = nullptr;
            delete temp;
            temp = nullptr;
            count--;
        }else{//normal process.
            Node* temp = tail_pointer;
            while(temp->getNextLink() != head_pointer){
                temp = temp->getNextLink();
            }
            head_pointer = temp;
            temp = temp->getNextLink();
            head_pointer->setNextLink(nullptr);
            delete temp;
            temp = nullptr;
            count--;
        }

    }
}
template<class T>
Queue<T>& Queue<T>::operator = (const Queue<T>& item){

    if(head_pointer != item.head_pointer){//checking if it is not the calling object
        head_pointer = item.head_pointer;//common ownership.
        tail_pointer = item.tail_pointer;
        count = item.count;
    }
    return(*this);

}
template<class T>
Queue<T>& Queue<T>::operator = (Queue<T>&& item){
    if(head_pointer != item.head_pointer){//checking if it is not the calling object
        head_pointer = item.head_pointer;//transfering the ownership.
        tail_pointer = item.tail_pointer;
        count = item.count;
        item.head_pointer = nullptr;
        item.tail_pointer = nullptr;
        item.count = 0;
    }
    return(*this);
}
#endif